import string

### DO NOT MODIFY THIS FUNCTION ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print('Loading word list from file...')
    # inFile: file
    in_file = open(file_name, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    print('  ', len(word_list), 'words loaded.')
    in_file.close()
    return word_list

### DO NOT MODIFY THIS FUNCTION ###
def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

### DO NOT MODIFY THIS FUNCTION ###
def get_story_string():
    """
    Returns: a joke in encrypted text.
    """
    f = open("/mnt/c/Users/drsin/.vscode/repos/OSSU/004 - Introduction to Computer Science and Programming using Python/Week 5 Object Oriented Programming/week-5-object-oriented-programming/PSet6/story.txt", "r")
    story = str(f.read())
    f.close()
    return story

WORDLIST_FILENAME = "/mnt/c/Users/drsin/.vscode/repos/OSSU/004 - Introduction to Computer Science and Programming using Python/Week 5 Object Oriented Programming/week-5-object-oriented-programming/PSet6/words.txt"

class Message(object):
    ### DO NOT MODIFY THIS METHOD ###
    def __init__(self, text):
        '''
        Initializes a Message object
                
        text (string): the message's text

        a Message object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words
        '''
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)

    ### DO NOT MODIFY THIS METHOD ###
    def get_message_text(self):
        '''
        Used to safely access self.message_text outside of the class
        
        Returns: self.message_text
        '''
        return self.message_text

    ### DO NOT MODIFY THIS METHOD ###
    def get_valid_words(self):
        '''
        Used to safely access a copy of self.valid_words outside of the class
        
        Returns: a COPY of self.valid_words
        '''
        return self.valid_words[:]
        
    def build_shift_dict(self, shift):
        '''
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift. The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.        
        
        shift (integer): the amount by which to shift every letter of the 
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        '''
        # alphabet_dict = {}
        # for i in string.ascii_letters:
        #     alphabet_dict[i] = string.ascii_letters.find(i)
        # shift_dict = alphabet_dict.copy()
        # for i in shift_dict:
        #     shift_dict[i] = shift_dict.get(i) + shift
        #     if i in string.ascii_uppercase:
        #         shift_dict[i] -= 26
        #     if shift_dict[i] < 0:
        #         shift_dict[i] += 26
        # return shift_dict
        az = "abcdefghijklmnopqrstuvwxyz"
        AZ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        keys = []
        vals_az = []
        vals_AZ = []
        
        for i in range(26):
            keys.append(i)
            vals_az.append(az[i])
            vals_AZ.append(AZ[i])

        shift_keys = list(az) + list(AZ)
        shift_vals = vals_az[shift:] + vals_az[:shift] + vals_AZ[shift:] + vals_AZ[:shift]

        self.shift_aZ = dict(zip(shift_keys, shift_vals))
        
        return self.shift_aZ


    def apply_shift(self, shift):
        '''
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift        
        
        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
             down the alphabet by the input shift
        '''
        # message_copy = self.get_message_text()
        # shift_dict = self.build_shift_dict(shift)
        # shift_dict_keys = list(shift_dict.keys())
        # shift_dict_values = list(shift_dict.values())
        # new_message = ""
        # for i in message_copy:
        #     if i in string.ascii_letters:
        #         i_pos = shift_dict.get(i)
        #         if i in string.ascii_lowercase:
        #             if i_pos < 0:
        #                 shift_dict[i] += 26
        #             new_message += shift_dict_keys[i_pos]                
        #         if i in string.ascii_uppercase:
        #             if i_pos < 26:
        #                 shift_dict[i] += 26
        #             if i_pos > 52:
        #                 shift_dict[i] -= 26
        #             new_message += shift_dict_keys[i_pos].upper()
        #     else:
        #         new_message += i
        # return new_message
        message_text = self.message_text

        univ_lst = list(string.digits) + list(string.punctuation) + list(" ")
        shifted_dict = self.build_shift_dict(shift)
        message_lst = list(message_text)
        
        cipher_lst = [shifted_dict[char] if char not in univ_lst else char for char in message_lst]
        self.cipher_text = "".join(cipher_lst)
        
        return self.cipher_text

class PlaintextMessage(Message):
    def __init__(self, text, shift):
        '''
        Initializes a PlaintextMessage object        
        
        text (string): the message's text
        shift (integer): the shift associated with this message

        A PlaintextMessage object inherits from Message and has five attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
            self.shift (integer, determined by input shift)
            self.encrypting_dict (dictionary, built using shift)
            self.message_text_encrypted (string, created using shift)

        Hint: consider using the parent class constructor so less 
        code is repeated
        '''
        Message.__init__(self, text)
        self.shift = shift
        self.encrypting_dict = self.build_shift_dict(shift)
        self.message_text_encrypted = self.apply_shift(shift)

    def get_shift(self):
        '''
        Used to safely access self.shift outside of the class
        
        Returns: self.shift
        '''
        return self.shift

    def get_encrypting_dict(self):
        '''
        Used to safely access a copy self.encrypting_dict outside of the class
        
        Returns: a COPY of self.encrypting_dict
        '''
        return self.encrypting_dict.copy()

    def get_message_text_encrypted(self):
        '''
        Used to safely access self.message_text_encrypted outside of the class
        
        Returns: self.message_text_encrypted
        '''
        return self.message_text_encrypted

    def change_shift(self, shift):
        '''
        Changes self.shift of the PlaintextMessage and updates other 
        attributes determined by shift (ie. self.encrypting_dict and 
        message_text_encrypted).
        
        shift (integer): the new shift that should be associated with this message.
        0 <= shift < 26

        Returns: nothing
        '''
        self.shift = shift
        self.encrypting_dict = self.build_shift_dict(shift)
        self.message_text_encrypted = self.apply_shift(shift)


class CiphertextMessage(Message):
    def __init__(self, text):
        '''
        Initializes a CiphertextMessage object
                
        text (string): the message's text

        a CiphertextMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''
        Message.__init__(self, text)

    def decrypt_message(self):
        '''
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value 
        for decrypting it.

        Note: if multiple shifts are  equally good such that they all create 
        the maximum number of you may choose any of those shifts (and their
        corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        '''
        num_of_words = 0
        score = 0
        
        for i in range(26):
            words_to_decrypt = (self.apply_shift(i)).split(" ")
            words_num = 0
            for word in words_to_decrypt:
                if is_word(self.valid_words, word):
                    words_num += 1
            if words_num > num_of_words:
                num_of_words = words_num
                score = i
        
        decrypted = "".join(self.apply_shift(score))
        
        return score, decrypted
            

# #Example test case (PlaintextMessage)
# plaintext = PlaintextMessage('hello', 2)
# print('Expected Output: jgnnq')
# print('Actual Output:', plaintext.get_message_text_encrypted())
# print(plaintext.shift)
# plaintext.change_shift(28)
# print(plaintext.shift)

# #Example test case (CiphertextMessage)
# ciphertext = CiphertextMessage('jgnnq')
# print('Expected Output:', (24, 'hello'))
# print('Actual Output:', ciphertext.decrypt_message())

# message = Message("hello")
# print(message.apply_shift(0))

def decryptStory():
    ciphertext = CiphertextMessage(get_story_string())
    return ciphertext.decrypt_message()