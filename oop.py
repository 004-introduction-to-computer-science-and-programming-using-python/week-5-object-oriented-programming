# each data object has 3 things
# 1. a type
# 2. internal data representation
# 3. set of procedures for interaction with the object
# 
# data objects are abstractions with 2 things:
# internal representation through data attributes
# a way to interface with the data typically through methods
# 
# destroy objects through del or allow python garbage collector to destroy inaccessable objects
# 
# use oop to abstract data into packages that can be worked upon
# divide and conquer implementation and testing
# easy to reuse code
# 
# class Name(parent class):
#   attributes
# 
# name would be subclass and parent class would be superclass
# 
# attributes are data and procedures that belong to the class
# need to define how to create an instance of the class 
# use 
# def __init__(self, args to create an isntance of class):
#   self.arg = args
# 
# c = Name(args)
# dont need to put self when creating instance of a class
# 
# methods are functions that are specifically designed to work with that class
# methods take self in as first argument
# 
# __str__ can be used to define what the class itself will return 
# 
# isInstance() will check if a created instance is an instance of a particular class
# 
# can override +, -, and ==
# 
# generator yield() is used to stop and start execution and wait for return values
# custom iterable that returns the next value using __next__ 
# similar to range() 


def genPrimes():
    primes = []   # primes generated so far
    last = 1      # last number tried
    while True:
        last += 1
        for p in primes:
            if last % p == 0:
                break
        else:
            primes.append(last)
            yield last